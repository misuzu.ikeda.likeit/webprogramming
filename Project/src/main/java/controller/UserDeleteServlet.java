package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserDeleteServlet() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // セッション情報を取得
    HttpSession session = request.getSession();
    User loginCheck = (User) session.getAttribute("userInfo");

    // ログインセッションがない場合
    if (loginCheck == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    try {
      String id = request.getParameter("id");

      // idを引数にして、idに紐づくユーザ情報を取得する
      UserDao userDao = new UserDao();
      User deleteUserInfo = userDao.findByDeleteUserInfo(id);

      // ユーザ情報をリクエストスコープにセットしてjspにフォワード
      request.setAttribute("deleteUserInfo", deleteUserInfo);

      // jspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
      dispatcher.forward(request, response);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    try {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String id = request.getParameter("user-id");

      // 確認用：idをコンソールに出力
      System.out.println(id);

      UserDao userDao = new UserDao();
      userDao.deleteUser(id);

      // リダイレクト
      response.sendRedirect("UserListServlet");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
