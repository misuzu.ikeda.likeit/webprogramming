package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserAddServlet() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // セッション情報を取得
    HttpSession session = request.getSession();
    User loginCheck = (User) session.getAttribute("userInfo");

    // ログインセッションがない場合
    if (loginCheck == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    // jspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    try {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("user-loginid");
      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("password-confirm");
      String userName = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");

      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      UserDao userDao = new UserDao();

      // 生年月日が未入力の場合
      if (birthDate.equals("")) {
        // エラーメッセージ
        request.setAttribute("errMsg", "入力された内容は正しくありません");

        // フォームの値を取得
        User inputHistoryInfo = new User(loginId, userName);
        request.setAttribute("inputHistoryInfo", inputHistoryInfo);

        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      Date dateBirthDate = Date.valueOf(birthDate);

      // すでに登録済みのログインIdの場合
      if (userDao.findByLoginId(loginId) != null) {
        // エラーメッセージ
        request.setAttribute("errMsg", "入力された内容は正しくありません");

        // フォームの値を取得
        User inputHistoryInfo = new User(loginId, userName, dateBirthDate);
        request.setAttribute("inputHistoryInfo", inputHistoryInfo);

        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      // パスワードと確認用のパスワードが違った場合
      if (!(password.equals(passwordConfirm))) {
        // エラーメッセージ
        request.setAttribute("errMsg", "入力された内容は正しくありません");

        // フォームの値を取得
        User inputHistoryInfo = new User(loginId, userName, dateBirthDate);
        request.setAttribute("inputHistoryInfo", inputHistoryInfo);

        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      if (loginId.equals("") || password.equals("") || passwordConfirm.equals("")
          || userName.equals("")) {
        // エラーメッセージ
        request.setAttribute("errMsg", "入力された内容は正しくありません");

        // フォームの値を取得
        User inputHistoryInfo = new User(loginId, userName, dateBirthDate);
        request.setAttribute("inputHistoryInfo", inputHistoryInfo);

        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
        return;
      }

      PasswordEncorder passwordEncorder = new PasswordEncorder();
      String encodePass = passwordEncorder.encordPassword(password);

      userDao.createUser(loginId, userName, birthDate, encodePass);

      // リダイレクト
      response.sendRedirect("UserListServlet");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
