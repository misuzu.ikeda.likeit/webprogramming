package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserUpdateServlet() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // セッション情報を取得
    HttpSession session = request.getSession();
    User loginCheck = (User) session.getAttribute("userInfo");

    // ログインセッションがない場合
    if (loginCheck == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    String id = request.getParameter("id");

    // idを引数にして、idに紐づくユーザ情報を取得する
    UserDao userDao = new UserDao();
    User updateUserInfo = userDao.findByUpdateUserInfo(id);

    // ユーザ情報をリクエストスコープにセットしてjspにフォワード
    request.setAttribute("updateUserInfo", updateUserInfo);

    // jspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    try {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String id = request.getParameter("user-id");
      String loginId = request.getParameter("login-id");
      String userName = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");
      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("password-confirm");

      // 確認用：idをコンソールに出力
      System.out.println(id);

      UserDao userDao = new UserDao();

      // 更新失敗時のリクエストパラメータの入力項目を取得
      int intId = Integer.parseInt(id);

      // 生年月日が未入力の場合
      if (birthDate.equals("")) {
        // エラーメッセージ
        request.setAttribute("errMsg", "入力された内容は正しくありません");

        // フォームの値を代入
        User inputHistoryInfo = new User(intId, loginId, userName);
        request.setAttribute("updateUserInfo", inputHistoryInfo);

        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      }

      Date dateBirthDate = Date.valueOf(birthDate);

      // パスワードと確認用のパスワードが違った場合
      if (!(password.equals(passwordConfirm))) {
        // エラーメッセージ
        request.setAttribute("errMsg", "入力された内容は正しくありません");

        // フォームの値を代入
        User inputHistoryInfo = new User(intId, loginId, userName, dateBirthDate);
        request.setAttribute("updateUserInfo", inputHistoryInfo);

        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      }

      if (password.equals("") && passwordConfirm.equals("")) {

        // 名前が未入力の場合
        if (userName.equals("")) {
          // エラーメッセージ
          request.setAttribute("errMsg", "入力された内容は正しくありません");

          // フォームの値を取得
          User inputHistoryInfo = new User(intId, loginId, userName, dateBirthDate);
          request.setAttribute("updateUserInfo", inputHistoryInfo);

          // フォワード
          RequestDispatcher dispatcher =
              request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
          dispatcher.forward(request, response);
          return;
        }

        userDao.updateUserExceptPassword(id, userName, birthDate);

        // リダイレクト
        response.sendRedirect("UserListServlet");
        return;
      }

      if (userName.equals("") || (password.equals("") && !(passwordConfirm.equals("")))
          || (!(password.equals("")) && passwordConfirm.equals(""))) {
        // エラーメッセージ
        request.setAttribute("errMsg", "入力された内容は正しくありません");

        // フォームの値を取得
        User inputHistoryInfo = new User(intId, loginId, userName, dateBirthDate);
        request.setAttribute("updateUserInfo", inputHistoryInfo);

        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      }

      PasswordEncorder passwordEncorder = new PasswordEncorder();
      String encodePass = passwordEncorder.encordPassword(password);

      userDao.updateUser(id, userName, birthDate,encodePass);

      // リダイレクト
      response.sendRedirect("UserListServlet");

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
