package controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserListServlet() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // セッション情報を取得
    HttpSession session = request.getSession();
    User loginCheck = (User) session.getAttribute("userInfo");

    // ログインセッションがない場合
    if (loginCheck == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    // ユーザ一覧情報を取得
    UserDao userDao = new UserDao();
    List<User> userList = userDao.findAll();

    // リクエストスコープにユーザ一覧情報をセット
    request.setAttribute("userList", userList);

    // ユーザ一覧のjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);
  }

  /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    try {
      // 実装済：検索処理全般
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("user-loginid");
      String userName = request.getParameter("user-name");
      String startDate = request.getParameter("date-start");
      String endDate = request.getParameter("date-end");

      UserDao userDao = new UserDao();
      List<User> serchUserList = userDao.searchUser(loginId, userName, startDate, endDate);

      // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("userList", serchUserList);

      // 実装済：どんな条件で検索したかわかるように画面に入力値を表示させること

      if (!(loginId.equals("")) || !(userName.equals(""))) {
        User inputSearchInfo = new User(loginId, userName);
        request.setAttribute("searchUserInfo", inputSearchInfo);
      }

      if (!(startDate.equals("")) || !(endDate.equals(""))) {
        Date sinceDate = Date.valueOf(startDate);
        Date utilDate = Date.valueOf(endDate);

        // フォームの値を代入
        User inputSearchInfo = new User(loginId, userName, sinceDate, utilDate);
        request.setAttribute("searchUserInfo", inputSearchInfo);
      }

      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
