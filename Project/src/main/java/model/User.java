package model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class User implements Serializable {
  private int id;
  private String loginId;
  private String name;
  private Date birthDate;
  private String password;
  private boolean isAdmin;
  private Timestamp createDate;
  private Timestamp updateDate;

  private Date startDate;
  private Date endDate;

  public User() {}

  // ログインID検索のためのコンストラクタ
  public User(String loginId) {
    this.loginId = loginId;
  }

  // ログインセッションを保存するためのコンストラクタ
  public User(String loginId, String name) {
    this.loginId = loginId;
    this.name = name;
  }

  // 新規登録失敗時のコンストラクタ
  public User(String loginId, String name, Date birthDate) {
    this.loginId = loginId;
    this.name = name;
    this.birthDate = birthDate;
  }

  // 詳細画面で取得する情報のためのコンストラクタ
  public User(String loginId, String name, Date birthDate, String password, Timestamp createDate,
      Timestamp updateDate) {
    this.loginId = loginId;
    this.name = name;
    this.birthDate = birthDate;
    this.password = password;
    this.createDate = createDate;
    this.updateDate = updateDate;
  }

  // データ更新用のコンストラクタ
  public User(int id, String loginId, String name, Date birthDate, String password) {
    this.id = id;
    this.loginId = loginId;
    this.name = name;
    this.birthDate = birthDate;
    this.password = password;
  }

  // ユーザーの更新可能な情報用のコンストラクタ
  public User(int id, String loginId, String name, Date birthDate) {
    this.id = id;
    this.loginId = loginId;
    this.name = name;
    this.birthDate = birthDate;
  }

  // 更新失敗の際のコンストラクタ
  public User(int id, String loginId, String name) {
    this.id = id;
    this.loginId = loginId;
    this.name = name;
  }

  // 削除するユーザー情報取得のためのコンストラクタ
  public User(int id, String loginId) {
    this.id = id;
    this.loginId = loginId;
  }

  public User(int id, String loginId, String name, Date birthDate, String password, boolean isAdmin,
      Timestamp createDate, Timestamp updateDate) {
    super();
    this.id = id;
    this.loginId = loginId;
    this.name = name;
    this.birthDate = birthDate;
    this.password = password;
    this.isAdmin = isAdmin;
    this.createDate = createDate;
    this.updateDate = updateDate;
  }

  // ユーザー一覧検索用のコンストラクタ
  public User(String loginId, String name, Date startDate, Date endDate) {
    this.loginId = loginId;
    this.name = name;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public int getId() {
    return id;
  }

  public String getLoginId() {
    return loginId;
  }

  public String getName() {
    return name;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public String getPassword() {
    return password;
  }

  public boolean isAdmin() {
    return isAdmin;
  }

  public Timestamp getCreateDate() {
    return createDate;
  }

  public Timestamp getUpdateDate() {
    return updateDate;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setAdmin(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }

  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }

  public void setUpdateDate(Timestamp updateDate) {
    this.updateDate = updateDate;
  }

  public Date getStartDate() {
    return startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

}
