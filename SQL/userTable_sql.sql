﻿CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

USE usermanagement;

CREATE TABLE user(
   id SERIAL PRIMARY KEY AUTO_INCREMENT,
   login_id  varchar(255) UNIQUE NOT NULL,
   name varchar(255) NOT NULL,
   birth_date DATE NOT NULL,
   password varchar(255) NOT NULL,
   is_admin  boolean NOT NULL default 0,
   create_date DATETIME NOT NULL,
   update_date DATETIME NOT NULL
);

INSERT INTO user
    (login_id, name, birth_date, password, is_admin, create_date, update_date)
VALUES('admin','管理者','2022-01-01','password', 1, now(), now());

